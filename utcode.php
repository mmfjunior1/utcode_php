<?php

$types = array('boolean' 	=> 'b',
				'integer' 	=> 'i',
				'double'	=> 'f',
				'string'	=> 's',
				'NULL' 		=> 'n',
				'array' 	=> 'l');

function utcode_encode($obj,$initial = true) {
	global $types;

	$utCodeStr 	= "";
	$final		= $initial == true ? 'e' : '';
	
	if($initial) {
		$utCodeStr = "ut:";	
	}	
	
	$decodedJson = @json_decode($obj,true);
	
	if($decodedJson !==null) {
		return utcode_encode($decodedJson);
	}
	
	if(is_array($obj)){
		foreach($obj as $key=>$value) {
			$end 		= ""; 
			$type 		= $types[gettype($value)];
			$length		= null;
			if($value === false) {
				$value = '0';
			}
			if(!is_array($value)) {
				$length 	= strlen($value);
			}
			switch($type) {
				case "i":
				case "f":
					$end 	= "e";
					$length = null;
				break;
				case 'b':
					$length = null;
				break;
				case "l":
					$length = null;
					if(is_int($key)) {
						$utCodeStr.="l:".utcode_encode($value,false)."e";
					}
					else
					{
						$lengthKey = strlen($key);
						$utCodeStr.="k".$lengthKey.":".$key."l:".utcode_encode($value,false)."e";	
					}
					
				break;
			}
			if(is_array($value)) {
				utcode_encode($value);
				continue;
			}
			
			$lengthKey = strlen($key);
			if(is_int($key)) {
				$utCodeStr.=$type.$length.":".$value.$end;
			}
			else
			{
				$lengthKey = strlen($key);
				$utCodeStr.="k".$lengthKey.":".$key.$type.$length.":".$value.$end;	
			}
		}
		
		if($utCodeStr == 'ut:') {
			return false;
		}
		
		return $utCodeStr.$final;
	}
	return false;
}


$array 	= array("body" => "","header"=>array("action"=>"utos.es.api.utos.timestamp","etag"=>"000"));
$json 	= '{"body": "","header": {"action": "utos.es.api.utos.timestamp","etag": "000"}}';
echo utcode_encode($json).chr(10);
echo utcode_encode($array).chr(10);
